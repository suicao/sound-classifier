import tensorflow as tf
from sklearn.metrics import precision_recall_fscore_support
from data_load import Data
import numpy as np

data = Data()
features, train_x, train_y, test_x, test_y = data.features, data.train_x, data.train_y, data.test_x, data.test_y
training_epochs = 3000
n_dim = features.shape[1]
n_classes = 2
n_hidden_units_one = 280
n_hidden_units_two = 300
sd = 1 / np.sqrt(n_dim)
learning_rate = 0.01
X = tf.placeholder(tf.float32, [None, n_dim])
Y = tf.placeholder(tf.float32, [None, n_classes])

W_1 = tf.Variable(tf.random_normal([n_dim, n_hidden_units_one], mean=0, stddev=sd))
b_1 = tf.Variable(tf.random_normal([n_hidden_units_one], mean=0, stddev=sd))
h_1 = tf.nn.tanh(tf.matmul(X, W_1) + b_1)

W_2 = tf.Variable(tf.random_normal([n_hidden_units_one, n_hidden_units_two], mean=0, stddev=sd))
b_2 = tf.Variable(tf.random_normal([n_hidden_units_two], mean=0, stddev=sd))
h_2 = tf.nn.sigmoid(tf.matmul(h_1, W_2) + b_2)

W = tf.Variable(tf.random_normal([n_hidden_units_two, n_classes], mean=0, stddev=sd))
b = tf.Variable(tf.random_normal([n_classes], mean=0, stddev=sd))
logits = tf.matmul(h_2, W) + b
y_ = tf.nn.softmax(logits)

# cost_function = tf.reduce_mean(-tf.reduce_sum(Y * tf.log(y_), reduction_indices=[1]))
cost_function = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=Y, logits=logits))
optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost_function)
init = tf.initialize_all_variables()

correct_prediction = tf.equal(tf.argmax(y_, 1), tf.argmax(Y, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
cost_history = np.empty(shape=[1], dtype=float)
y_true, y_pred = None, None

print(X.name, y_.name)
with tf.Session() as sess:
    sess.run(init)
    for epoch in range(training_epochs):
        _, cost = sess.run([optimizer, cost_function], feed_dict={X: train_x, Y: train_y})
        if epoch % 100 == 0:
            _, cost, acc = sess.run([optimizer, cost_function, accuracy], feed_dict={X: test_x, Y: test_y})
            print("Epoch", epoch, "Accuracy:", round(acc, 3))
        cost_history = np.append(cost_history, cost)

    saver = tf.train.Saver(tf.global_variables())
    for variable in tf.global_variables():
        tensor = tf.constant(variable.eval())
        tf.assign(variable, tensor, name="nWeights")
    tf.train.write_graph(sess.graph_def, "infer", 'graph.pb', as_text=False)
    checkpoint_path = "./ckpt/model.ckpt"
    saver.save(sess, checkpoint_path, global_step=1, latest_filename="checkpoint_1")
