import glob
import os
import librosa
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import specgram

plt.style.use('ggplot')

plt.rcParams['font.family'] = 'serif'
plt.rcParams['font.serif'] = 'Ubuntu'
plt.rcParams['font.monospace'] = 'Ubuntu Mono'
plt.rcParams['font.size'] = 12
plt.rcParams['axes.labelsize'] = 11
plt.rcParams['axes.labelweight'] = 'bold'
plt.rcParams['axes.titlesize'] = 14
plt.rcParams['xtick.labelsize'] = 10
plt.rcParams['ytick.labelsize'] = 10
plt.rcParams['legend.fontsize'] = 11
plt.rcParams['figure.titlesize'] = 13


class Data:
    def __init__(self, sub_dirs=['hello', 'trash'], split=4, mode='train'):

        parent_dir = 'sound-data'

        self.features, labels = self.parse_audio_files(parent_dir, sub_dirs)
        n_features = len(self.features)
        self.labels = self.one_hot_encode(labels)
        print(labels.shape)
        if mode == "train":
            self.train_x = np.array([f for idx, f in enumerate(self.features) if idx % split != 0])
            self.train_y = np.array([l for idx, l in enumerate(self.labels) if idx % split != 0])
            self.test_x = np.array([f for idx, f in enumerate(self.features) if idx % split == 0])
            self.test_y = np.array([l for idx, l in enumerate(self.labels) if idx % split == 0])

            print(len(self.train_x), len(self.train_y), len(self.test_x), len(self.test_y), n_features)
        else:
            self.infer_x = self.features

    @staticmethod
    def load_sound_files(file_paths):
        raw_sounds = []
        for fp in file_paths:
            X, sr = librosa.load(fp)
            raw_sounds.append(X)
        return raw_sounds

    @staticmethod
    def plot_waves(sound_names, raw_sounds):
        i = 1
        fig = plt.figure(figsize=(25, 60), dpi=900)
        for n, f in zip(sound_names, raw_sounds):
            plt.subplot(10, 1, i)
            librosa.display.waveplot(np.array(f), sr=22050)
            plt.title(n.title())
            i += 1
        plt.suptitle('Figure 1: Waveplot', x=0.5, y=0.915, fontsize=18)
        plt.show()

    @staticmethod
    def plot_specgram(sound_names, raw_sounds):
        i = 1
        fig = plt.figure(figsize=(25, 60), dpi=900)
        for n, f in zip(sound_names, raw_sounds):
            plt.subplot(10, 1, i)
            specgram(np.array(f), Fs=22050)
            plt.title(n.title())
            i += 1
        plt.suptitle('Figure 2: Spectrogram', x=0.5, y=0.915, fontsize=18)
        plt.show()

    @staticmethod
    def plot_log_power_specgram(sound_names, raw_sounds):
        i = 1
        fig = plt.figure(figsize=(25, 60), dpi=900)
        for n, f in zip(sound_names, raw_sounds):
            plt.subplot(10, 1, i)
            D = librosa.logamplitude(np.abs(librosa.stft(f)) ** 2, ref_power=np.max)
            librosa.display.specshow(D, x_axis='time', y_axis='log')
            plt.title(n.title())
            i += 1
        plt.suptitle('Figure 3: Log power spectrogram', x=0.5, y=0.915, fontsize=18)
        plt.show()

    @staticmethod
    def extract_feature(file_name):
        X, sample_rate = librosa.load(file_name)
        stft = np.abs(librosa.stft(X))
        mfccs = np.mean(librosa.feature.mfcc(y=X, sr=sample_rate, n_mfcc=40).T, axis=0)
        chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=sample_rate).T, axis=0)
        mel = np.mean(librosa.feature.melspectrogram(X, sr=sample_rate).T, axis=0)
        contrast = np.mean(librosa.feature.spectral_contrast(S=stft, sr=sample_rate).T, axis=0)
        tonnetz = np.mean(librosa.feature.tonnetz(y=librosa.effects.harmonic(X), sr=sample_rate).T, axis=0)
        return mfccs, chroma, mel, contrast, tonnetz

    def parse_audio_files(self, parent_dir, sub_dirs, file_ext='*.wav'):
        print("sub dirs: ", sub_dirs)
        features, labels = np.empty((0, 193)), np.empty(0)
        for label, sub_dir in enumerate(sub_dirs):
            for fn in glob.glob(os.path.join(parent_dir, sub_dir, file_ext)):
                print(fn)
                mfccs, chroma, mel, contrast, tonnetz = self.extract_feature(fn)
                ext_features = np.hstack([mfccs, chroma, mel, contrast, tonnetz])
                features = np.vstack([features, ext_features])
                # labels = np.append(labels, fn.split('/')[2].split('-')[1])
                labels = np.append(labels, [sub_dirs.index(sub_dir)])
        return np.array(features), np.array(labels, dtype=np.int)

    def one_hot_encode(self, labels):
        n_labels = len(labels)
        n_unique_labels = len(np.unique(labels))
        one_hot_encode = np.zeros((n_labels, n_unique_labels))
        one_hot_encode[np.arange(n_labels), labels] = 1
        return one_hot_encode


if __name__ == "__main__":
    data = Data()
    train_x = data.train_x
