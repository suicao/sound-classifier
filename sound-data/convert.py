import os

data_dir = 'negative'

save_data_dir = '8knegative'

index = 1
for f in os.listdir(data_dir):
    input_file = os.path.join(data_dir, f)
    output_file = os.path.join(save_data_dir, str(index) + '.wav')
    os.system("sox {} -r 8000 -c 1 {}".format(input_file, output_file))
    index += 1
